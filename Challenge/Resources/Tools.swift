//
//  Tools.swift
//  Challenge
//
//  Created by Julio César on 02/02/2020.
//  Copyright © 2020 jcreyes. All rights reserved.
//

import Foundation


func celsius(fromKelvin: Double) -> Double {
	let convertion = 273.15
	let celsius = fromKelvin - convertion

	return celsius.rounded(digits: 1)
}

func kelvin(fromCelsius: Double) -> Double {
	let convertion = 273.15
	let kelvin = fromCelsius + convertion

	return kelvin.rounded(digits: 1)
}

func unixToDateComplete(date: Double) -> String {
	let date = Date(timeIntervalSince1970: date)

	let dayTimePeriodFormatter = DateFormatter()
	dayTimePeriodFormatter.dateFormat = "EEEE MMM dd YYYY hh:mm a"
	let dateString = dayTimePeriodFormatter.string(from: date)

	return dateString
}


func unixToDate(date: Int) -> String {
	let date = Date(timeIntervalSince1970: Double(date))

	let dayTimePeriodFormatter = DateFormatter()
	dayTimePeriodFormatter.dateFormat = "dd MMMM YYYY"
	let dateString = dayTimePeriodFormatter.string(from: date)

	return dateString
}

func unixToDateHourAndMinute(date: Int) -> String {
	let date = Date(timeIntervalSince1970: Double(date))

	let dayTimePeriodFormatter = DateFormatter()
	dayTimePeriodFormatter.dateFormat = "hh:mm"
	let dateString = dayTimePeriodFormatter.string(from: date)

	return dateString
}


func unixToDateHour(date: Int) -> String {
	let date = Date(timeIntervalSince1970: Double(date))

	let dayTimePeriodFormatter = DateFormatter()
	dayTimePeriodFormatter.dateFormat = "hh"
	let dateString = dayTimePeriodFormatter.string(from: date)

	return dateString
}

func unixToDateDay(date: Int) -> String {
	let date = Date(timeIntervalSince1970: Double(date))

	let dayTimePeriodFormatter = DateFormatter()
	dayTimePeriodFormatter.dateFormat = "EEEE"
	let dateString = dayTimePeriodFormatter.string(from: date)

	return dateString
}

func getCardinalPoint(angle: Int) -> String {
	var direction = ""
	switch angle {
		case 0..<12:
			direction = "N"
			break
		case 12..<34:
			direction = "NNE"
			break
		case 34..<56:
			direction = "NE"
			break
		case 56..<78:
			direction = "ENE"
			break
		case 78..<102:
			direction = "E"
			break
		case 102..<124:
			direction = "ESE"
			break
		case 124..<146:
			direction = "SE"
			break
		case 146..<168:
			direction = "SSE"
			break
		case 168..<192:
			direction = "S"
			break
		case 192..<214:
			direction = "SSW"
			break
		case 214..<236:
			direction = "SW"
			break
		case 236..<258:
			direction = "WSW"
			break
		case 258..<282:
			direction = "W"
			break
		case 282..<304:
			direction = "WNW"
			break
		case 304..<326:
			direction = "NW"
			break
		case 326..<348:
			direction = "NNW"
			break
		case 348..<360:
			direction = "N"
			break
		default:
			direction = ""
	}

	return direction
}

func image(fromDescription desc: String, date: Int) -> String {
	var imageName = ""
	let hour: Int! = Int(unixToDateHour(date: date))
	switch desc {
		case "broken clouds":
			imageName = "nubes"
		case "moderate rain":
			imageName = "lluvia"
		case "overcast clouds":
			imageName = "nubes"
		case "few clouds":
			if hour >= 6 && hour < 20 {
				imageName = "nube-diurna"
			} else {
				imageName = "nube-nocturna"
			}
		case "mist":
			imageName = "niebla"
		case "fog":
			imageName = "niebla"
		case "scattered clouds":
			if hour >= 6 && hour < 20 {
				imageName = "nube-diurna"
			} else {
				imageName = "nube-nocturna"
		}
		case "clear sky":
			if hour >= 6 && hour < 20 {
				imageName = "sol"
			} else {
				imageName = "luna"
		}
		case "light rain":
			imageName = "lluvia"
		case "light snow":
			imageName = "nieve"
		case "snow":
			imageName = "nieve"
		case "heavy intensity rain":
			imageName = "lluvia-fuerte"
		case "thunderstorm":
			imageName = "perno"
		default:
			if hour >= 6 && hour < 20 {
				imageName = "sol"
			} else {
				imageName = ""
		}
	}

	return imageName
}


extension Double {

	func rounded(digits: Int) -> Double {
		let multiplier = pow(10.0, Double(digits))
		return (self * multiplier).rounded() / multiplier
	}

}
