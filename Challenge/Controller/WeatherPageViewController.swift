//
//  WeatherPageViewController.swift
//  Challenge
//
//  Created by Julio César on 02/02/2020.
//  Copyright © 2020 jcreyes. All rights reserved.
//

import UIKit

class WeatherPageViewController: UIPageViewController {


	var currentWeather: [DataWeather] = []
	var weatherForecast: [DataForecast] = []
	let urlSession: URLSession = URLSession(configuration: .default)
	let apiKey = "55ec057d2de0ddd9c356937be979968d"
	let cities: [Int] = [3435910, 5128638, 2147714, 3646738, 6359304, 3451190]


    override func viewDidLoad() {
        super.viewDidLoad()

		self.dataSource = self
		self.setDataWeather()
		self.setDataForecast()

		if let startViewController = self.pageViewController(atIndex: 0) {
			self.setViewControllers([startViewController], direction: .forward, animated: true, completion: nil)
		}
    }


	func fecthWeathers(fromCity cityId: Int) {
		let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?id=\(cityId)&appid=\(apiKey)")!
		let task = urlSession.dataTask(with: url) { (data, response, error) in
			guard let data = data else { return }
			DispatchQueue.main.async {
				do {
					let decoder = JSONDecoder()
					let resultWeather: CurrentWeather = try decoder.decode(CurrentWeather.self, from: data)
					print("WEATHER:\n\(resultWeather)\n")
					self.currentWeather.append(DataWeather(index: self.currentWeather.count, weather: resultWeather))

					if self.currentWeather.count == 1 {
						if let startViewController = self.pageViewController(atIndex: 0) {
							self.setViewControllers([startViewController], direction: .forward, animated: true, completion: nil)
						}
					}
					return
				} catch {
					print("ERROR: \(error.localizedDescription)")
				}

				self.showMessage(message: "No se ha podido recibir información del servidor, intente nuevamente.")
			}
		}
		task.resume()
	}

	func setDataWeather() {
		for index in 0 ..< cities.count {
			self.fecthWeathers(fromCity: cities[index])
		}
	}

	func fetchForecast(fromCity cityId: Int) {
		let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?id=\(cityId)&appid=\(apiKey)")!
		let task = urlSession.dataTask(with: url) { (data, response, error) in
			guard let data = data else { return }
			DispatchQueue.main.async {
				do {
					let decoder = JSONDecoder()
					let resultForecast: WeatherForecast = try decoder.decode(WeatherForecast.self, from: data)
					print("FORECAST:\n\(resultForecast)\n")
					self.weatherForecast.append(DataForecast(index: self.weatherForecast.count, forecast: resultForecast))

					if self.weatherForecast.count == 1 {
						if let startViewController = self.pageViewController(atIndex: 0) {
							self.setViewControllers([startViewController], direction: .forward, animated: true, completion: nil)
						}
					}
					return
				} catch {
					print("ERROR: \(error.localizedDescription)")
				}

				self.showMessage(message: "No se ha podido recibir información del servidor, intente nuevamente.")
			}
		}
		task.resume()
	}

	func setDataForecast() {
		for index in 0 ..< cities.count {
			self.fetchForecast(fromCity: cities[index])
		}
	}

	func showMessage(message: String) {
		let alert = UIAlertController(title: "Error de descarga", message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Reintentar", style: .default) { (UIAlertAction) in
			self.fecthWeathers(fromCity: self.cities[0])
			self.fetchForecast(fromCity: self.cities[0])
		})
		alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

		self.present(alert, animated: true)
	}

}


extension WeatherPageViewController: UIPageViewControllerDataSource {

	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		var index = (viewController as! WeatherContentViewController).currentWeather!.index
		index += 1
		return self.pageViewController(atIndex: index)
	}


	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		var index = (viewController as! WeatherContentViewController).currentWeather!.index

		index -= 1
		return self.pageViewController(atIndex: index)
	}


	func pageViewController(atIndex: Int) -> WeatherContentViewController? {
		if atIndex == NSNotFound || atIndex < 0 || atIndex >= self.currentWeather.count || atIndex >= self.weatherForecast.count {
			return nil
		}
		if let pageContentViewController = storyboard?.instantiateViewController(identifier: "WeatherPageContent") as? WeatherContentViewController {
			pageContentViewController.currentWeather = self.currentWeather[atIndex]
			pageContentViewController.weatherForecast = self.weatherForecast[atIndex]

			return pageContentViewController
		}
		return nil
	}

}
