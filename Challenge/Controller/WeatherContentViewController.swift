//
//  WeatherContentViewController.swift
//  Challenge
//
//  Created by Julio César on 02/02/2020.
//  Copyright © 2020 jcreyes. All rights reserved.
//

import UIKit

class WeatherContentViewController: UIViewController {

	@IBOutlet weak var currentDayLabel: UILabel!
	@IBOutlet weak var currentDateLabel: UILabel!

	@IBOutlet weak var currentCityLabel: UILabel!
	@IBOutlet weak var currentTempLabel: UILabel!
	@IBOutlet weak var currentWeatherLabel: UILabel!
	@IBOutlet weak var currentTempMinLabel: UILabel!
	@IBOutlet weak var currentTempMaxLabel: UILabel!
	@IBOutlet weak var currentWeatherIconImage: UIImageView!

	@IBOutlet weak var nextHour1Label: UILabel!
	@IBOutlet weak var nextWeather1Image: UIImageView!
	@IBOutlet weak var nextTemp1Label: UILabel!

	@IBOutlet weak var nextHour2Label: UILabel!
	@IBOutlet weak var nextWeather2Image: UIImageView!
	@IBOutlet weak var nextTemp2Label: UILabel!

	@IBOutlet weak var nextHour3Label: UILabel!
	@IBOutlet weak var nextWeather3Image: UIImageView!
	@IBOutlet weak var nextTemp3Label: UILabel!

	@IBOutlet weak var nextHour4Label: UILabel!
	@IBOutlet weak var nextWeather4Image: UIImageView!
	@IBOutlet weak var nextTemp4Label: UILabel!

	@IBOutlet weak var nextHour5Label: UILabel!
	@IBOutlet weak var nextWeather5Image: UIImageView!
	@IBOutlet weak var nextTemp5Label: UILabel!

	@IBOutlet weak var nextHour6Label: UILabel!
	@IBOutlet weak var nextWeather6Image: UIImageView!
	@IBOutlet weak var nextTemp6Label: UILabel!

	@IBOutlet weak var nextHour7Label: UILabel!
	@IBOutlet weak var nextWeather7Image: UIImageView!
	@IBOutlet weak var nextTemp7Label: UILabel!

	@IBOutlet weak var nextHour8Label: UILabel!
	@IBOutlet weak var nextWeather8Image: UIImageView!
	@IBOutlet weak var nextTemp8Label: UILabel!

	@IBOutlet weak var feelsLikeLabel: UILabel!
	@IBOutlet weak var pressureLabel: UILabel!
	@IBOutlet weak var humidityLabel: UILabel!
	@IBOutlet weak var windLabel: UILabel!
	@IBOutlet weak var sunriseLabel: UILabel!
	@IBOutlet weak var sunsetLabel: UILabel!

	@IBOutlet weak var nextDay1Label: UILabel!
	@IBOutlet weak var nextDate1Label: UILabel!
	@IBOutlet weak var nextAverageTemp1Label: UILabel!
	@IBOutlet weak var nextTempMin1Label: UILabel!
	@IBOutlet weak var nextTempMax1Label: UILabel!
	@IBOutlet weak var nextIcon1Image: UIImageView!

	@IBOutlet weak var nextDay2Label: UILabel!
	@IBOutlet weak var nextDate2Label: UILabel!
	@IBOutlet weak var nextAverageTemp2Label: UILabel!
	@IBOutlet weak var nextTempMin2Label: UILabel!
	@IBOutlet weak var nextTempMax2Label: UILabel!
	@IBOutlet weak var nextIcon2Image: UIImageView!

	@IBOutlet weak var nextDay3Label: UILabel!
	@IBOutlet weak var nextDate3Label: UILabel!
	@IBOutlet weak var nextAverageTemp3Label: UILabel!
	@IBOutlet weak var nextTempMin3Label: UILabel!
	@IBOutlet weak var nextTempMax3Label: UILabel!
	@IBOutlet weak var nextIcon3Image: UIImageView!

	@IBOutlet weak var nextDay4Label: UILabel!
	@IBOutlet weak var nextDate4Label: UILabel!
	@IBOutlet weak var nextAverageTemp4Label: UILabel!
	@IBOutlet weak var nextTempMin4Label: UILabel!
	@IBOutlet weak var nextTempMax4Label: UILabel!
	@IBOutlet weak var nextIcon4Image: UIImageView!

	@IBOutlet weak var nextDay5Label: UILabel!
	@IBOutlet weak var nextDate5Label: UILabel!
	@IBOutlet weak var nextAverageTemp5Label: UILabel!
	@IBOutlet weak var nextTempMin5Label: UILabel!
	@IBOutlet weak var nextTempMax5Label: UILabel!
	@IBOutlet weak var nextIcon5Image: UIImageView!

	@IBOutlet weak var pageControl: UIPageControl!


	var currentWeather: DataWeather?
	var weatherForecast: DataForecast?


    override func viewDidLoad() {
        super.viewDidLoad()

		self.pageControl.currentPage = self.currentWeather?.index ?? 0
		self.setData()
    }

	func setData() {
		if let dataW = self.currentWeather {
			if let dataF = self.weatherForecast {
				self.currentDayLabel.text = "\(unixToDateDay(date: dataW.weather.dt))"
				self.currentDateLabel.text = "\(unixToDate(date: dataW.weather.dt))"
				self.currentCityLabel.text = dataW.weather.name
				self.currentTempLabel.text = "\(celsius(fromKelvin: Double(dataW.weather.main.temp)))º"
				self.currentWeatherLabel.text = dataW.weather.weather.first!.description.capitalized
				self.currentTempMinLabel.text = "\(celsius(fromKelvin: Double(dataW.weather.main.temp_min)))º"
				self.currentTempMaxLabel.text = "\(celsius(fromKelvin: Double(dataW.weather.main.temp_max)))º"
				self.currentWeatherIconImage.image = UIImage(named: image(fromDescription: dataW.weather.weather.first!.description, date: dataW.weather.dt))
				self.feelsLikeLabel.text = "\(celsius(fromKelvin: Double(dataW.weather.main.feels_like)))º"
				self.pressureLabel.text = "\(dataW.weather.main.pressure) hPa"
				self.humidityLabel.text = "\(dataW.weather.main.humidity) %"
				self.windLabel.text = "\(getCardinalPoint(angle: dataW.weather.wind.deg)) \(dataW.weather.wind.speed) km/h"
				self.sunriseLabel.text = "\(unixToDateHourAndMinute(date: dataW.weather.sys.sunrise))"
				self.sunsetLabel.text = "\(unixToDateHourAndMinute(date: dataW.weather.sys.sunset))"


				self.nextHour1Label.text = "\(unixToDateHour(date: dataF.forecast.list[0].dt))"
				self.nextWeather1Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[0].weather.description, date: dataF.forecast.list[0].dt))
				self.nextTemp1Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[0].main.temp)))º"
				self.nextHour2Label.text = "\(unixToDateHour(date: dataF.forecast.list[1].dt))"
				self.nextWeather2Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[1].weather.first!.description, date: dataF.forecast.list[1].dt))
				self.nextTemp2Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[1].main.temp)))º"
				self.nextHour3Label.text = "\(unixToDateHour(date: dataF.forecast.list[2].dt))"
				self.nextWeather3Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[2].weather.first!.description, date: dataF.forecast.list[2].dt))
				self.nextTemp3Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[2].main.temp)))º"
				self.nextHour4Label.text = "\(unixToDateHour(date: dataF.forecast.list[3].dt))"
				self.nextWeather4Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[3].weather.first!.description, date: dataF.forecast.list[3].dt))
				self.nextTemp4Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[3].main.temp)))º"
				self.nextHour5Label.text = "\(unixToDateHour(date: dataF.forecast.list[4].dt))"
				self.nextWeather5Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[4].weather.first!.description, date: dataF.forecast.list[4].dt))
				self.nextTemp5Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[4].main.temp)))º"
				self.nextHour6Label.text = "\(unixToDateHour(date: dataF.forecast.list[5].dt))"
				self.nextWeather6Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[5].weather.first!.description, date: dataF.forecast.list[5].dt))
				self.nextTemp6Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[5].main.temp)))º"
				self.nextHour7Label.text = "\(unixToDateHour(date: dataF.forecast.list[6].dt))"
				self.nextWeather7Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[6].weather.first!.description, date: dataF.forecast.list[6].dt))
				self.nextTemp7Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[6].main.temp)))º"
				self.nextHour8Label.text = "\(unixToDateHour(date: dataF.forecast.list[7].dt))"
				self.nextWeather8Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[7].weather.first!.description, date: dataF.forecast.list[7].dt))
				self.nextTemp8Label.text = "\(celsius(fromKelvin: Double(dataF.forecast.list[7].main.temp)))º"

				self.nextDay1Label.text = "\(unixToDateDay(date: dataF.forecast.list[8].dt))"
				self.nextDate1Label.text = "\(unixToDate(date: dataF.forecast.list[8].dt))"
				self.nextAverageTemp1Label.text = "\(celsius(fromKelvin: dataF.forecast.list[8].main.temp))º"
				self.nextTempMin1Label.text = "\(celsius(fromKelvin: dataF.forecast.list[8].main.temp_min))º"
				self.nextTempMax1Label.text = "\(celsius(fromKelvin: dataF.forecast.list[8].main.temp_max))º"
				self.nextIcon1Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[8].weather.first!.description, date: dataF.forecast.list[8].dt))

				self.nextDay2Label.text = "\(unixToDateDay(date: dataF.forecast.list[12].dt))"
				self.nextDate2Label.text = "\(unixToDate(date: dataF.forecast.list[12].dt))"
				self.nextAverageTemp2Label.text = "\(celsius(fromKelvin: dataF.forecast.list[12].main.temp))º"
				self.nextTempMin2Label.text = "\(celsius(fromKelvin: dataF.forecast.list[12].main.temp_min))º"
				self.nextTempMax2Label.text = "\(celsius(fromKelvin: dataF.forecast.list[12].main.temp_max))º"
				self.nextIcon2Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[12].weather.first!.description, date: dataF.forecast.list[12].dt))

				self.nextDay3Label.text = "\(unixToDateDay(date: dataF.forecast.list[20].dt))"
				self.nextDate3Label.text = "\(unixToDate(date: dataF.forecast.list[20].dt))"
				self.nextAverageTemp3Label.text = "\(celsius(fromKelvin: dataF.forecast.list[20].main.temp))º"
				self.nextTempMin3Label.text = "\(celsius(fromKelvin: dataF.forecast.list[20].main.temp_min))º"
				self.nextTempMax3Label.text = "\(celsius(fromKelvin: dataF.forecast.list[20].main.temp_max))º"
				self.nextIcon3Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[20].weather.first!.description, date: dataF.forecast.list[20].dt))

				self.nextDay4Label.text = "\(unixToDateDay(date: dataF.forecast.list[28].dt))"
				self.nextDate4Label.text = "\(unixToDate(date: dataF.forecast.list[28].dt))"
				self.nextAverageTemp4Label.text = "\(celsius(fromKelvin: dataF.forecast.list[28].main.temp))º"
				self.nextTempMin4Label.text = "\(celsius(fromKelvin: dataF.forecast.list[28].main.temp_min))º"
				self.nextTempMax4Label.text = "\(celsius(fromKelvin: dataF.forecast.list[28].main.temp_max))º"
				self.nextIcon4Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[28].weather.first!.description, date: dataF.forecast.list[28].dt))

				self.nextDay5Label.text = "\(unixToDateDay(date: dataF.forecast.list[36].dt))"
				self.nextDate5Label.text = "\(unixToDate(date: dataF.forecast.list[36].dt))"
				self.nextAverageTemp5Label.text = "\(celsius(fromKelvin: dataF.forecast.list[36].main.temp))º"
				self.nextTempMin5Label.text = "\(celsius(fromKelvin: dataF.forecast.list[36].main.temp_min))º"
				self.nextTempMax5Label.text = "\(celsius(fromKelvin: dataF.forecast.list[36].main.temp_max))º"
				self.nextIcon5Image.image = UIImage(named: image(fromDescription: dataF.forecast.list[36].weather.first!.description, date: dataF.forecast.list[36].dt))
			}
		}
	}

}
