//
//  WeatherDataTypes.swift
//  Challenge
//
//  Created by Julio César on 07/02/2020.
//  Copyright © 2020 jcreyes. All rights reserved.
//


struct DataWeather {
	var index: Int
	var weather: CurrentWeather
}

struct CurrentWeather: Codable {
	var weather: [Weather]
	var main: Main
	var wind: Wind
	var clouds: Clouds
	var dt: Int
	var sys: Sys
	var name: String
}

struct Weather: Codable {
	var main: String
	var description: String
}

struct Main: Codable {
	var temp: Double
	var feels_like: Double
	var temp_min: Double
	var temp_max: Double
	var pressure: Int
	var humidity: Int
}

struct Wind: Codable {
	var speed: Double
	var deg: Int
}

struct Clouds: Codable {
	var all: Int
}

struct Sys: Codable {
	var sunrise: Int
	var sunset: Int
}



struct DataForecast {
	var index: Int
	var forecast: WeatherForecast
}

struct WeatherForecast: Codable {
	var cnt: Int
	var list: [List]
	var city: City
}

struct List: Codable {
	var dt: Int
	var main: Main
	var weather: [Weather]
	var clouds: Clouds
	var wind: Wind
}

struct City: Codable {
	var name: String
	var country: String
	var sunrise: Int
	var sunset: Int
}
